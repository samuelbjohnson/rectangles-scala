/**
	* Created by Samuel B. Johnson on 7/9/16.
	* Unless specified elsewhere, this code is copyright 2016
	* and is licensed under the terms of the AGPL 3.0
	*/

import com.samuelbjohnson.rectangles.Rectangle

import scala.io.{StdIn => in}

object CLI extends App {
	def readRectangle() = {
		println("Define a rectangle")
		print("Corner X: ")
		val x = in.readInt
		print("Corner Y: ")
		val y = in.readInt
		print("Width: ")
		val width = in.readInt
		print("Height: ")
		val height = in.readInt
		
		Rectangle((x, y), width, height)
	}
	
	var r1 = readRectangle()
	var r2 = readRectangle()
	
	if (r1.isContainedWithin(r2)) {
		println("The first rectangle is contained within the second")
	}
	if (r2.isContainedWithin(r1)) {
		println("The second rectangle is contained within the first")
	}
	if (r1.isAdjacentTo(r2)) {
		println("The rectangles are adjacent")
	}
	r1.findIntersections(r2) match {
		case Nil => 
			println("The rectangles do not intersect")
		case intersections =>
			val pretty = intersections map {p => s"(${p.x}, ${p.y})"}
			println(s"The rectangles intersect at the following points ${pretty.mkString(", ")}.")
	}
}
