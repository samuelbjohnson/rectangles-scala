package com.samuelbjohnson.rectangles

/**
	* Created by Samuel B. Johnson on 7/8/16.
	* Unless specified elsewhere, this code is copyright 2016
	* and is licensed under the terms of the AGPL 3.0
	*/

import scala.language.implicitConversions

case class Point(x: Int, y: Int) 
object Point {
	implicit def fromTuple2(point: (Int, Int)): Point = Point(point._1, point._2)
}

case class Dimension(value: Int) {
	value match {
		case x if x <= 0 => throw new IllegalArgumentException("Dimension value must be greater than 0")
		case _ =>
	}

	implicit def asInt = value
}
object Dimension {
	implicit def intDimension(v: Int): Dimension = Dimension(v)
}

case class Rectangle(corner: Point, width: Dimension, height: Dimension) {
	def surrounds(p: Point) = {
		this.surroundsX(p.x) && this.surroundsY(p.y)
	}
	
	def incidentPoint(p: Point) = {
		(incidentX(p.x) && includesY(p.y)) || (incidentY(p.y) && includesX(p.x))
	}

	def isContainedWithin(that: Rectangle) = {
		val corner1Inside = that.surrounds(corner)
		val corner2Inside = that.surrounds(farCorner)

		corner1Inside && corner2Inside
	}

	def isAdjacentTo(that: Rectangle) = {
		(
			that.incidentPoint(corner) || 
			that.incidentPoint(farCorner) || 
			incidentPoint(that.corner) || 
			incidentPoint(that.farCorner)
		) && this != that
	}

	def findIntersections(that: Rectangle) = isAdjacentTo(that) || this == that match {
		case true => 
			List.empty[Point]
		case false =>
			(intersectionPoints(that) filter { p =>
				incidentPoint(p) && that.incidentPoint(p)
			}).toList
	}
	
	private def farCorner = Point(corner.x + width.asInt, corner.y + height.asInt)
	
	private def intersectionPoints(that: Rectangle) = Set(
		corner,
		Point(corner.x, that.farCorner.y),
		Point(farCorner.x, that.corner.y),
		Point(farCorner.x, that.farCorner.y),
		that.corner,
		Point(that.corner.x, farCorner.y),
		Point(that.farCorner.x, corner.y),
		Point(that.farCorner.x, farCorner.y)
	)

	private def incidentX(x1: Int) = {
		x1 == corner.x || x1 == farCorner.x
	}

	private def incidentY(y1: Int) = {
		y1 == corner.y || y1 == farCorner.y
	}
	
	private def surroundsX(x1: Int) = {
		x1 > corner.x && x1 < farCorner.x
	}

	private def includesX(x1: Int) = {
		x1 >= corner.x && x1 <= farCorner.x
	}

	private def surroundsY(y1: Int) = {
		y1 > corner.y && y1 < farCorner.y
	}

	private def includesY(y1: Int) = {
		y1 >= corner.y && y1 < farCorner.y
	}
}
