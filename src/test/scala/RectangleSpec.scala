package com.samuelbjohnson.rectangles

import org.scalatest.{FlatSpec, Matchers}
import scala.language.implicitConversions

/**
	* Created by Samuel B. Johnson on 7/8/16.
	* Unless specified elsewhere, this code is copyright 2016
	* and is licensed under the terms of the AGPL 3.0
	*/

class PointSpec extends FlatSpec with Matchers {
	"A Dimension" should "throw an exception given a non-positive value" in {
		try {
			val x = Dimension(-1)
			fail("No exception was thrown for a negative value")
		} catch {
			case e: IllegalArgumentException =>
			case e: Exception => fail(s"An unexpected exception was thrown $e")
		}
	}
	it should "accept a positive value" in {
		try {
			val x = Dimension(1)
			x.value should equal (1)
		} catch {
			case _: Exception => fail()
		}
	}
	
	"A Point" should "support equality" in {
		Point(1, 1) should equal (Point(1, 1))
	}
}

class RectangleSpec extends FlatSpec with Matchers {
	val correctSurroundedPoints = Set(Point(6, 4))
	val correctIncidentPoints = Set(
		Point(0, 0), Point(6, 0), Point(12, 0),
		Point(0, 4),              Point(12, 4),
		Point(0, 8), Point(6, 8), Point(12, 8))
	
	val rectangle = new Rectangle((0, 0), 12, 8)
	val points = (for {
		x <- List(-1, 0, 6, 12, 13)
		y <- List(-1, 0, 4, 8, 9)
	} yield {
		Point(x, y)
	}).toSet
	val checkedSurroundedPoints = points filter { rectangle.surrounds(_) }
	val checkedIncidentPoints = points filter { rectangle.incidentPoint(_) }
	
	"Rectangle.surrounds()" should "return true for points surrounded by the rectangle" in {
		checkedSurroundedPoints should equal (correctSurroundedPoints)
	}
	
	it should "return false for points not surrounded by the rectangle" in {
		(checkedSurroundedPoints diff correctSurroundedPoints) shouldBe empty
	}

	"Rectangle.incidentPoint()" should "return true for points incident on the rectangle" in {
		checkedIncidentPoints should equal (correctIncidentPoints)
	}
	
	it should "return false for points not incident on the rectangle" in {
		(checkedIncidentPoints diff correctIncidentPoints) shouldBe empty
	}

	"Rectangle.isContainedWithin()" should "return false with both corners outside" in {
		rectangle.isContainedWithin(new Rectangle((1, 1), 10, 6)) should be (false)
		rectangle.isContainedWithin(new Rectangle((0, 1), 10, 6)) should be (false)
	}
	it should "return false with one corner outside" in {
		rectangle.isContainedWithin(new Rectangle((-1, -1), 10, 6)) should be (false)
	}
	it should "return false with one corner inside, one corner incident" in {
		rectangle.isContainedWithin(new Rectangle((0, 4), 10, 2)) should be (false)
	}
	it should "return false for identical rectangles" in {
		rectangle.isContainedWithin(rectangle) should be (false)
	}
	it should "return true with both corners inside" in {
		rectangle.isContainedWithin(new Rectangle((-1, -1), 14, 10)) should be (true)
	}

	"Rectangle.isAdjacentTo()" should "return false with both corners outside" in {
		rectangle.isAdjacentTo(new Rectangle((1, 1), 10, 6)) should be (false)
	}
	it should "return false with one corner outside" in {
		rectangle.isAdjacentTo(new Rectangle((-1, -1), 10, 6)) should be (false)
	}
	it should "return true with one corner inside, one corner incident" in {
		rectangle.isAdjacentTo(new Rectangle((0, 4), 10, 2)) should be (true)
	}
	it should "return false for identical rectangles" in {
		rectangle.isAdjacentTo(rectangle) should be (false)
	}
	it should "return false with both corners inside" in {
		rectangle.isAdjacentTo(new Rectangle((-1, -1), 14, 10)) should be (false)
	}
	it should "return true with both corners outside, 1 dimension incident" in {
		rectangle.isAdjacentTo(new Rectangle((0, 1), 10, 6)) should be (true)
	}

	"Rectangle.findIntersections()" should "return an empty list with both corners outside" in {
		rectangle.findIntersections(new Rectangle((1, 1), 10, 6)) shouldBe empty
	}
	it should "return intersections with one corner outside: one vertical, one horizontal intersection" in {
		val intersections = rectangle.findIntersections(new Rectangle((-1, -1), 10, 6))
		intersections should have length 2
		intersections should contain (Point(9, 0))
		intersections should contain (Point(0, 5))
	}
	it should "return intersections with one corner outside: two vertical intersections" in {
		val intersections = rectangle.findIntersections(new Rectangle((10, 2), 5, 5))
		intersections should have length 2
		intersections should contain (Point(12, 2))
		intersections should contain (Point(12, 7))
	}
	it should "return intersections with one corner outside: two horizontal intersections" in {
		val intersections = rectangle.findIntersections(new Rectangle((1, 6), 5, 5))
		intersections should have length 2
		intersections should contain (Point(1, 8))
		intersections should contain (Point(6, 8))
	}
	it should "return an empty list with one corner inside, one corner incident" in {
		val intersections = rectangle.findIntersections(new Rectangle((0, 4), 10, 2))
		intersections shouldBe empty
	}
	it should "return an empty list for identical rectangles" in {
		val intersections = rectangle.findIntersections(rectangle)
		intersections shouldBe empty
	}
	it should "return an empty list with both corners inside" in {
		val intersections = rectangle.findIntersections(new Rectangle((-1, -1), 14, 10))
		intersections shouldBe empty
	}
	it should "return an empty list with both corners outside, 1 dimension incident" in {
		val intersections = rectangle.findIntersections(new Rectangle((0, 1), 10, 6))
		intersections shouldBe empty
	}
}
